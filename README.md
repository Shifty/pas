<div align="left">
  <a href="pas-img">
    <img src="https://i.imgur.com/QM3EPsa.png" alt="Pas Icon" height="64" width="64" align="left">
  </a>
</div>

# Personal Audio Streamer

### A music bot for Discord

> Pas is currently a private bot, not available for public invitation.

### Config

You can create a Discord Application [here](https://discord.com/developers/applications/)
and a Spotify Web App [here](https://developer.spotify.com/dashboard/applications)

Create the file `static/config.json` and add the following values to it:

| Key | Description | Type | 
| :---: | ----------- | :---: |
| `debug` | Whether to catch exceptions | **bool** |
| `slim` | Setting this to false changes most responses to non-embeds | **bool** |
| `prefix_space` | Whether there should be a space after the prefix | **bool** |
| `prefix` | The default prefix the bot will respond to | **string** |
| `token` | Your Discord Application's token | **string** |
| `owners` | A list of user IDs to be marked as the bot's owners | **list** |
| `error_channel` | The ID of the channel to log errors in | **int** |
| `sp_client_id` | The client ID for your Spotify Web App | **string** |
| `sp_client_secret` | The client secret for your Spotify Web App | **string** |

For YouTube cookies, use
[Get cookies.txt](https://chrome.google.com/webstore/detail/get-cookiestxt/bgaddhkoddajcdgocldbbfleckgcbcid/) (for Chrome) or
[cookies.txt](https://addons.mozilla.org/en-US/firefox/addon/cookies-txt/) (for Firefox)
and put the file in `static/`. It should be named `cookies.txt`.

### Setup
*You may need to change the venv path depending on your OS*
```shell
$ python -m venv venv/
$ source venv/bin/activate
$ pip install -Ur requirements.txt
$ python run.py
```
**or**
```shell
$ ./run.sh
```
