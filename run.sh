#!/usr/bin/env bash

set -e

unset CDPATH
cd "$(dirname "$0")"

source "venv/bin/activate"
python -m pip install -Ur requirements.txt &>/dev/null
exec python ./run.py
