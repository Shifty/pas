import datetime

from core.stream import Query


async def queue(cmd, ctx):
    page_num = False
    if len(ctx.args) == 1:
        if ctx.args[0].isdigit():
            page_num = abs(int(ctx.args[0]))

    if not ctx.args or page_num:
        curr = None
        song_queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
        if ctx.msg.guild.id in cmd.bot.stream.currents:
            curr = cmd.bot.stream.currents[ctx.msg.guild.id]
        if song_queue.empty() and curr:
            response = ctx.response(0x3B88C3, '🎵 The queue is empty')
            if not cmd.bot.cfg.slim:
                response.raw.description = f'Currently playing: [{curr.title}]({curr.source_url})'
            else:
                response.raw += f'. Currently playing: {curr.title}'
            await response.send()
            return
        if song_queue.empty():
            response = ctx.response(0x3B88C3, '🎵 The queue is empty')
            await response.send()
            return

        full_song_list = await cmd.bot.stream.queue_to_list(song_queue)
        queue_duration = sum(s.duration for s in full_song_list)
        queue_duration = str(datetime.timedelta(seconds=queue_duration)).lstrip('0:')

        page = page_num if page_num else 1
        song_list, page = cmd.bot.utils.paginate(full_song_list, page)
        start_range, _ = (page - 1) * 10, page * 10
        info = f'Songs: {len(full_song_list)} | Duration: {queue_duration} | Page: {page}'

        if ctx.msg.guild.id in cmd.bot.stream.song_repeaters:
            info += ' | Song repeat on'
        elif ctx.msg.guild.id in cmd.bot.stream.queue_repeaters:
            info += ' | Queue repeat on'

        song_text = ''
        if ctx.msg.guild.id in cmd.bot.stream.currents:
            curr = cmd.bot.stream.currents[ctx.msg.guild.id]
            song_text += f'Currently playing: [{curr.title}]({curr.source_url})\n\n'

        for i, song in enumerate(song_list):
            title = cmd.bot.utils.shorten(song.title, 45, '...')
            req = cmd.bot.utils.shorten(song.requester.name, 20, '...')
            duration = str(datetime.timedelta(seconds=song.duration)).lstrip('0:')
            song_text += f'**{start_range + i + 1}.** [{title}]({song.source_url}) {duration} | {req}\n'

        response = ctx.response(0x3B88C3, '🎵 Music Queue', force_embed=True)
        response.raw.description = song_text
        response.raw.set_footer(text=info)
        await response.send()
        return

    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return

    query = Query.parse(cmd.bot, ' '.join(ctx.args))
    if query.is_spotify and not cmd.bot.stream.sp:
        await ctx.reply(0xBE1931, '❗ Spotify support is not enabled')
        return

    if query.is_multi:
        response = ctx.response(0xFFCC66, '💽 Queueing playlist...')
    elif query.is_share or query.is_url:
        response = ctx.response(0xFFCC66, '💽 Queueing song...')
    else:
        response = ctx.response(0xFFCC66, '💽 Searching...')
    init_msg = await response.send()

    results, error = await query.get_results(cmd, ctx)
    if error:
        response = ctx.response(0xBE1931, f'❗ {error}')
        await response.send(init_msg)
        return
    if not results:
        response = ctx.response(0x696969, '🔍 No results')
        await response.send(init_msg)
        return

    if query.is_multi:
        for song in results:
            await cmd.bot.stream.queue_song(song)
        s = 's' if len(results) > 1 else ''
        response = ctx.response(0x66CC66, f'✅ Queued {len(results)} song{s}')

    else:
        song = results
        await cmd.bot.stream.queue_song(song)
        if not cmd.bot.cfg.slim:
            response = ctx.response(0x66CC66)
            response.raw.add_field(name='✅ Queued song', value=f'[{song.title}]({song.url})')
            response.raw.set_thumbnail(url=song.thumbnail)
            duration = str(datetime.timedelta(seconds=song.duration)).lstrip('0:')
            response.raw.set_footer(text=f'Duration: {duration}')
        else:
            response = ctx.response(title=f'✅ Queued {song.title}')

    await response.send(init_msg)
