async def restart(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.msg.guild.voice_client.is_playing():
        await ctx.reply(0xBE1931, '❗ The player is not active')
        return

    curr_song = cmd.bot.stream.currents.get(ctx.msg.guild.id)
    await cmd.bot.stream.top_queue_song(curr_song)
    ctx.msg.guild.voice_client.stop()

    await ctx.reply(0x3B88C3, '↩️ Restarted song')
