async def skip(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    current = cmd.bot.stream.currents.get(ctx.msg.guild.id)
    if not current:
        await ctx.reply(0xBE1931, '❗ Nothing currently playing')
        return

    ctx.msg.guild.voice_client.stop()
    await ctx.reply(0x66CC66, f'✅ Skipping: {current.title}')
