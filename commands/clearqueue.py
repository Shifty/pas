from asyncio.queues import Queue


async def clearqueue(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return

    if ctx.msg.guild.id in cmd.bot.stream.queues:
        player = cmd.bot.stream.players.get(ctx.msg.guild.id)
        if player:
            while not player.queue.empty():
                await player.queue.get()

    await ctx.reply(0x66CC66, '✅ Queue cleared')
