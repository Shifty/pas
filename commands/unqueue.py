from asyncio.queues import Queue


async def unqueue(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.args:
        await ctx.reply(0xBE1931, '❗ Nothing inputted')
        return
    if not ctx.args[0].isdigit():
        await ctx.reply(0xBE1931, '❗ Input must be a positive number')
        return

    queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
    if queue.empty():
        await ctx.reply(0xBE1931, '❗ The queue is empty')
        return
    song_index = abs(int(ctx.args[0]))
    if song_index > queue.qsize():
        await ctx.reply(0xBE1931, '❗ Target song is out of range')
        return
    if song_index >= 1:
        song_index -= 1

    song_list = await cmd.bot.stream.queue_to_list(queue)
    song = song_list[song_index]
    song_list.remove(song)

    new_queue = Queue()
    for list_item in song_list:
        await new_queue.put(list_item)
    cmd.bot.stream.queues.update({ctx.msg.guild.id: new_queue})

    await ctx.reply(0x66CC66, f'✅ Removed {song.title}')
