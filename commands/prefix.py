async def prefix(cmd, ctx):
    if not ctx.args:
        default = cmd.bot.cfg.prefix
        current = cmd.bot.db.prefix(ctx.msg.guild.id)
        await ctx.reply(0x3B88C3, f'ℹ The prefix is `{current or default}`')
        return
    if not ctx.msg.author.guild_permissions.manage_guild:
        await ctx.reply(0xBE1931, '⛔ Manage Server required')
        return

    new_prefix = ''.join(ctx.args)
    if '\n' in new_prefix:
        await ctx.reply(0xBE1931, '❗ Prefix can\'t contain line breaks')
        return

    cmd.bot.db.update(ctx.msg.guild.id, prefix=new_prefix)
    await ctx.reply(0x66CC66, f'✅ Prefix set to `{new_prefix}`')
