import inspect

exe_icon = 'https://i.imgur.com/Lw7mmnX.png'
err_icon = 'https://i.imgur.com/S7aUuLU.png'


async def evaluate(_cmd, ctx):
    if not ctx.args:
        await ctx.reply(0xBE1931, '❗ Nothing inputted')
        return

    try:
        execution = ' '.join(ctx.args)
        output = eval(execution)
        if inspect.isawaitable(output):
            output = await output
        response = ctx.response(0x38BE6E, force_embed=True)
        response.raw.description = f'```py\n{output}\n```'
        response.raw.set_author(name='Executed', icon_url=exe_icon)
    except Exception as e:
        response = ctx.response(0xBE1931, force_embed=True)
        response.raw.description = str(e)
        response.raw.set_author(name='Error', icon_url=err_icon)

    await response.send()
