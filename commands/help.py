# noinspection PyShadowingBuiltins
async def help(cmd, ctx):
    if ctx.args:
        cmd_name = ctx.args[0].lower()
        if cmd_name in cmd.bot.alts:
            cmd_name = cmd.bot.alts[cmd_name]
        if cmd_name not in cmd.bot.commands:
            await ctx.reply(0x696969, '🔍 Command not found')
            return

        command = cmd.bot.commands[cmd_name]
        response = ctx.response(0x4f208c, f'📄 {command.name.upper()} Help', force_embed=True)
        prefix = cmd.bot.db.prefix(ctx.msg.guild.id)
        usage = f'{prefix}{command.name} {command.usage}'
        response.raw.add_field(name='Usage Example', value=f'`{usage.strip()}`', inline=False)
        response.raw.add_field(name='Description', value=f'```\n{command.desc}\n```', inline=False)
        if command.alts:
            response.raw.add_field(name='Aliases', value=f'```\n{", ".join(command.alts)}\n```', inline=False)

    else:
        response = ctx.response(0x4f208c, force_embed=True)
        if ctx.msg.author.id in cmd.bot.cfg.owners:
            command_list = sorted(cmd.bot.commands)
        else:
            command_list = sorted(filter(lambda c: not cmd.bot.commands[c].owner, cmd.bot.commands))
        command_list = '\n'.join([f'- {command.upper()}' for command in command_list])
        response.raw.add_field(name='Command List', value=f'```yml\n{command_list}\n```')
        prefix = cmd.bot.db.prefix(ctx.msg.guild.id)
        response.raw.set_footer(text=f'Type {prefix}{cmd.name} [command] for more help')

    await response.send()
