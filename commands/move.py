from asyncio.queues import Queue


async def move(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if len(ctx.args) < 2:
        await ctx.reply(0xBE1931, '❗ Two arguments are required')
        return
    if not (ctx.args[0].isdigit() and ctx.args[1].isdigit()):
        await ctx.reply(0xBE1931, '❗ Inputs must be positive numbers')
        return

    queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
    if queue.empty():
        await ctx.reply(0xBE1931, '❗ The queue is empty')
        return

    song_list = await cmd.bot.stream.queue_to_list(queue)
    target_song = abs(int(ctx.args[0]))
    target_pos = abs(int(ctx.args[1]))
    if target_song > len(song_list):
        await ctx.reply(0xBE1931, '❗ Target song is out of range')
        return
    if target_pos > len(song_list):
        await ctx.reply(0xBE1931, '❗ Target position is out of range')
        return
    if target_song == target_pos:
        await ctx.reply(0xBE1931, '❗ Target song is already in that position')
        return

    song = song_list.pop(target_song - 1)
    song_list.insert(target_pos - 1, song)

    new_queue = Queue()
    for song_item in song_list:
        await new_queue.put(song_item)
    cmd.bot.stream.queues.update({ctx.msg.guild.id: new_queue})

    await ctx.reply(0x66CC66, f'✅ Moved song {target_song} to position {target_pos}')
