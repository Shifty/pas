import importlib


async def reload(cmd, ctx):
    if not ctx.args:
        cmd.bot.commands.clear()
        cmd.bot.init_commands()
        await ctx.reply(0x66CC66, '✅ Reloaded commands')
        return

    cmd_name = ctx.args[0].lower()
    if cmd_name in cmd.bot.alts:
        cmd_name = cmd.bot.alts[cmd_name]
    if cmd_name not in cmd.bot.commands:
        await ctx.reply(0x696969, '🔍 Command not found')
        return

    command = cmd.bot.commands[cmd_name]
    importlib.reload(command.command)
    await ctx.reply(0x66CC66, f'✅ Reloaded `{command.name}`.')
