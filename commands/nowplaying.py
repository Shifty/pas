import datetime


async def nowplaying(cmd, ctx):
    if ctx.msg.guild.id not in cmd.bot.stream.currents:
        await ctx.reply(0xBE1931, '❗ Nothing currently playing')
        return

    song = cmd.bot.stream.currents[ctx.msg.guild.id]
    if not cmd.bot.cfg.slim:
        response = ctx.response(0x3B88C3)
        response.raw.add_field(name='🎵 Now Playing', value=f'[{song.title}]({song.source_url})')
        response.raw.set_thumbnail(url=song.thumbnail)
        duration = str(datetime.timedelta(seconds=song.duration)).lstrip('0:')
        response.raw.set_footer(text=f'Duration: {duration} | Requester: {song.requester}')
    else:
        response = ctx.response(title=f'🎵 Now Playing: {song.title}')
    await response.send()
