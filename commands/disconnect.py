async def disconnect(cmd, ctx):
    user_vc = ctx.msg.author.voice
    if not user_vc:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    bot_vc = ctx.msg.guild.voice_client
    if not bot_vc:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if bot_vc.channel.id != user_vc.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return

    await bot_vc.disconnect()
    cmd.bot.stream.clean_up(ctx.msg.guild.id)
    await ctx.reply(0x77B255, '✅ Disconnected')
