import asyncio


async def connect(_cmd, ctx):
    user_vc = ctx.msg.author.voice
    if not user_vc:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    bot_vc = ctx.msg.guild.voice_client
    if bot_vc:
        if bot_vc.channel.id != user_vc.channel.id:
            await ctx.reply(0xBE1931, '❗ I am already in another channel')
            return
        else:
            await ctx.reply(0xBE1931, '❗ We are already in the same channel')
            return
    if not ctx.msg.guild.me.permissions_in(user_vc.channel).connect:
        await ctx.reply(0xBE1931, '❗ I cannot connect to that channel')
        return
    if not ctx.msg.guild.me.permissions_in(user_vc.channel).speak:
        await ctx.reply(0xBE1931, '❗ I cannot speak in that channel')
        return

    try:
        await user_vc.channel.connect(reconnect=False)
    except asyncio.TimeoutError:
        if ctx.msg.guild.voice_client:
            await bot_vc.disconnect()
        await ctx.reply(0xBE1931, '❗ A connection timeout occurred')
        return

    if ctx.args and ctx.args[0].lower() == '--quiet':
        return
    await ctx.reply(0x77B255, f'✅ Connected to {user_vc.channel.name}')
