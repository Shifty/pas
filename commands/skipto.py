from asyncio.queues import Queue


async def move(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.args:
        await ctx.reply(0xBE1931, '❗ Nothing inputted')
        return
    if not ctx.args[0].isdigit():
        await ctx.reply(0xBE1931, '❗ Input must be a positive number')
        return

    queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
    if queue.empty():
        await ctx.reply(0xBE1931, '❗ The queue is empty')
        return

    song_list = await cmd.bot.stream.queue_to_list(queue)
    target_song = abs(int(ctx.args[0]))
    if target_song > len(song_list):
        await ctx.reply(0xBE1931, '❗ Target song is out of range')
        return

    song_list = song_list[:target_song - 1]
    new_queue = Queue()
    for song_item in song_list:
        await new_queue.put(song_item)
    cmd.bot.stream.queues.update({ctx.msg.guild.id: new_queue})

    if ctx.msg.guild.voice_client.is_playing():
        ctx.msg.guild.voice_client.stop()

    await ctx.reply(0x66CC66, f'✅ Skipped to song {target_song}')
