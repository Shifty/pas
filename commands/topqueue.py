import datetime

from core.stream import Query, Song


async def topqueue(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.args:
        await ctx.reply(0xBE1931, '❗ Nothing inputted')
        return

    query = Query.parse(cmd.bot, ' '.join(ctx.args))
    if query.is_spotify and not cmd.bot.stream.sp:
        await ctx.reply(0xBE1931, '❗ Spotify support is not enabled')
        return

    if query.is_multi:
        response = ctx.response(0xFFCC66, '💽 Queueing playlist...')
    elif query.is_share or query.is_url:
        response = ctx.response(0xFFCC66, '💽 Queueing song...')
    else:
        response = ctx.response(0xFFCC66, '💽 Searching...')
    init_msg = await response.send()

    results, error = await query.get_results(cmd, ctx)
    if error:
        response = ctx.response(0xBE1931, f'❗ {error}')
        await response.send(init_msg)
        return
    if not results:
        response = ctx.response(0x696969, '🔍 No results')
        await response.send(init_msg)
        return

    if query.is_multi:
        for song in reversed(results):
            await cmd.bot.stream.top_queue_song(song)
        s = 's' if len(results) > 1 else ''
        response = ctx.response(0x66CC66, f'✅ Top-Queued {len(results)} song{s}')

    else:
        song = results
        await cmd.bot.stream.top_queue_song(song)
        if not cmd.bot.cfg.slim:
            response = ctx.response(0x66CC66)
            response.raw.add_field(name='✅ Top-Queued song', value=f'[{song.title}]({song.url})')
            response.raw.set_thumbnail(url=song.thumbnail)
            duration = str(datetime.timedelta(seconds=song.duration)).lstrip('0:')
            response.raw.set_footer(text=f'Duration: {duration}')
        else:
            response = ctx.response(title=f'✅ Top-Queued {song.title}')

    await response.send(init_msg)
