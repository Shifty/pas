import asyncio

import discord


async def removemessages(cmd, ctx):
    # noinspection PyBroadException
    try:
        bulk = ctx.msg.guild.me.permissions_in(ctx.msg.channel).manage_messages
        await ctx.msg.channel.purge(limit=100, check=lambda m: m.author.id == cmd.bot.user.id, bulk=bulk)
    except Exception:
        await ctx.reply(0xBE1931, '❗ There was an error while removing messages')
        return

    msg = await ctx.reply(0x66CC66, '✅ Removed messages')
    await asyncio.sleep(3)
    try:
        await msg.delete()
    except discord.NotFound:
        pass
