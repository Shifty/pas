async def repeatsong(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return

    if ctx.msg.guild.id in cmd.bot.stream.song_repeaters:
        cmd.bot.stream.song_repeaters.remove(ctx.msg.guild.id)
        await ctx.reply(0x3B88C3, '➡ Song repeat disabled')
    else:
        cmd.bot.stream.song_repeaters.append(ctx.msg.guild.id)
        await ctx.reply(0x3B88C3, '🔂 Song repeat enabled')
