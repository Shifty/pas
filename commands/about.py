import sys
from datetime import datetime

import discord

repo_url = 'https://gitlab.com/Shifty/pas'


async def about(cmd, ctx):
    response = discord.Embed(color=0x4f208c)
    info = f'Version: **{cmd.bot.cfg.version.version}**\n'
    latest_build = datetime.fromtimestamp(cmd.bot.cfg.version.timestamp).strftime('%Y.%m.%d')
    info += f'Latest Build: **{latest_build}**\n'
    info += f'Language: **Python {sys.version.split()[0]}**\n'
    info += f'Library: **discord.py {discord.__version__}**\n'
    info += f'View On [Gitlab]({repo_url})'
    response.add_field(name='Personal Audio Streamer', value=info)
    response.set_thumbnail(url=ctx.get_avatar(cmd.bot.user))
    await ctx.msg.channel.send(embed=response)
