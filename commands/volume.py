def volume_icon(target_volume):
    icons = {(1, 33): '🔈', (34, 66): '🔉', (67, 100): '🔊'}
    choice = None
    for (low, high), icon in icons.items():
        if low <= target_volume <= high:
            choice = icon
            break
    return choice


async def volume(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.msg.guild.voice_client.source:
        await ctx.reply(0xBE1931, '❗ I am not playing anything')
        return
    if not ctx.args:
        current_volume = int(ctx.msg.guild.voice_client.source.volume * 100)
        await ctx.reply(0xCCD6DD, f'{volume_icon(current_volume)} The volume is at {current_volume}%')
        return
    target_volume = ctx.args[0].rstrip('%')
    if not target_volume.isdigit():
        await ctx.reply(0xBE1931, '❗ Input must be a positive number')
        return
    target_volume = abs(int(target_volume))
    if target_volume > 100:
        await ctx.reply(0xBE1931, '❗ The max volume is 100')
        return
    if target_volume == 0:
        await ctx.reply(0xBE1931, '❗ The minimum volume is 1')
        return

    ctx.msg.guild.voice_client.source.volume = target_volume / 100
    cmd.bot.db.update(ctx.msg.guild.id, volume=target_volume)
    await ctx.reply(0xCCD6DD, f'{volume_icon(target_volume)} Volume changed to {target_volume}%')
