async def history(cmd, ctx):
    song_history = cmd.bot.stream.histories.get(ctx.msg.guild.id)
    if not song_history:
        await ctx.reply(0x3B88C3, f'ℹ No track history')
        return

    response = ctx.response(0x5DADEC, '🎶 Last 10 songs played', force_embed=True)
    response.raw.description = '\n'.join([f'**{i + 1}.** {s}' for i, s in enumerate(song_history)])
    await response.send()
