import secrets
from asyncio.queues import Queue


async def shuffle(cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return

    queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
    if queue.empty():
        await ctx.reply(0x3B88C3, '🎵 The queue is empty')
        return

    new_queue = Queue()
    song_list = await cmd.bot.stream.queue_to_list(queue)
    while song_list:
        await new_queue.put(song_list.pop(secrets.randbelow(len(song_list))))
    cmd.bot.stream.queues.update({ctx.msg.guild.id: new_queue})

    await ctx.reply(0x3B88C3, f'🔀 Shuffled {new_queue.qsize()} songs')
