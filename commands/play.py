from copy import copy

from core.stream import Player


async def play(cmd, ctx):
    user_vc = ctx.msg.author.voice
    if not user_vc:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    bot_vc = ctx.msg.guild.voice_client
    if not bot_vc:
        connect_ctx = copy(ctx)
        connect_ctx.args = ['--quiet']
        await cmd.bot.commands.get('connect').execute(connect_ctx)
    elif bot_vc.channel.id != user_vc.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.msg.guild.voice_client:
        return

    if ctx.args:
        await cmd.bot.commands.get('queue').execute(ctx)
        queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
        if queue.empty():
            return
        if ctx.msg.guild.voice_client.is_playing():
            return

    curr_song = cmd.bot.stream.currents.get(ctx.msg.guild.id)
    if curr_song and ctx.msg.guild.voice_client.is_paused():
        ctx.msg.guild.voice_client.resume()
        await ctx.reply(0x3B88C3, '⏸ Music resumed')
        return
    if ctx.msg.guild.voice_client.is_playing():
        await ctx.reply(0xBE1931, '❗ The player is not paused')
        return
    queue = cmd.bot.stream.get_queue(ctx.msg.guild.id)
    if queue.empty():
        await ctx.reply(0xBE1931, '❗ The queue is empty')
        return

    player = Player.new(cmd, ctx)
    await player.start()
