import subprocess


def from_output(output):
    return '' if len(output) <= 1 else f"```\n{output.decode('utf-8')}\n```"


async def sysexec(_cmd, ctx):
    if not ctx.args:
        await ctx.reply(0xBE1931, '❗ Nothing inputted')
        return

    try:
        process = subprocess.run(ctx.args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        await ctx.msg.add_reaction('✔')
        response = from_output(process.stdout)
    except (OSError, subprocess.SubprocessError) as e:
        await ctx.msg.add_reaction('❗')
        response = f'```\n{e}\n```'

    await ctx.msg.channel.send(response)
