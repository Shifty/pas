async def resume(_cmd, ctx):
    if not ctx.msg.author.voice:
        await ctx.reply(0xBE1931, '❗ You are not in a voice channel')
        return
    if not ctx.msg.guild.voice_client:
        await ctx.reply(0xBE1931, '❗ I am not in a voice channel')
        return
    if ctx.msg.author.voice.channel.id != ctx.msg.guild.voice_client.channel.id:
        await ctx.reply(0xBE1931, '❗ We are not in the same channel')
        return
    if not ctx.msg.guild.voice_client.is_paused():
        await ctx.reply(0xBE1931, '❗ The player is not paused')
        return

    ctx.msg.guild.voice_client.resume()
    await ctx.reply(0x3B88C3, '⏸ Music resumed')
