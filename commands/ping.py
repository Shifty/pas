async def ping(cmd, ctx):
    response = ctx.response(0x3B88C3, f'📶 **Ping:** {int(cmd.bot.latency * 1000)}ms')
    vc = ctx.msg.guild.voice_client
    if vc:
        try:
            if not cmd.bot.cfg.slim:
                response.raw.description = f'**Voice Ping:** {int(vc.average_latency * 1000)}ms'
            else:
                response.raw += f'\n**Voice Ping:** {int(vc.average_latency * 1000)}ms'
        except ZeroDivisionError:
            pass
    await response.send()
