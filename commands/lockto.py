async def lockto(cmd, ctx):
    if not ctx.msg.author.guild_permissions.manage_guild:
        await ctx.reply(0xBE1931, '⛔ Manage Server required')
        return

    current = cmd.bot.db.select(ctx.msg.guild.id, 'channels') or ""
    current = [int(c) for c in current.split(',') if c]
    if not ctx.args:
        response = ctx.response(0x3B88C3, force_embed=True)
        current = [c.mention for c in [ctx.msg.guild.get_channel(x) for x in current if x] if c]
        if current:
            response.raw.add_field(name='ℹ Designated Music Channels', value=' '.join(list(current)))
        else:
            response.raw.title = 'ℹ No designated music channels'
        await response.send()
        return

    if not ctx.msg.channel_mentions:
        await ctx.reply(0xBE1931, '❗ Specify a channel via mention')
        return

    channel = ctx.msg.channel_mentions[0]
    if channel.id in current:
        current.remove(channel.id)
        await ctx.reply(0x66CC66, f'✅ Removed #{channel.name} from the approved channels')
    else:
        current.append(channel.id)
        await ctx.reply(0x66CC66, f'✅ Added #{channel.mention} to the approved channels')
    cmd.bot.db.update(ctx.msg.guild.id, channels=','.join([str(c) for c in current]))
