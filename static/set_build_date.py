import json
import os
from datetime import datetime

# This is just an easier way for me to bump the build date.

base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
with open(os.path.join(base_path, 'static/version.json'), 'r+', encoding='utf-8') as version_file:
    ver_data = json.load(version_file)

    build_date = int(datetime.utcnow().timestamp())
    ver_data.update({'build_date': build_date})

    version_file.truncate(0)
    version_file.seek(0)
    json.dump(ver_data, version_file, indent=2)

print(f'Set build date to {build_date}')
