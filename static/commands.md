## Commands

> ### ~about
>
> Displays information about the bot.

> ### ~clearqueue
>
> Clears the entire queue. This does not stop the current song.
>
> **Aliases:** clear

> ### ~connect
>
> Connects the bot to a channel.
>
> **Aliases:** join, summon

> ### ~disconnect
>
> Disconnects the bot from a channel and clears the queue.
>
> **Aliases:** dc, leave

> ### ~evaluate
>
> Executes raw Python code. Use with caution. (Bot Owner Only)
>
> **Usage:** `~evaluate print("Hello!")`
>
> **Aliases:** eval, py

> ### ~help
>
> Shows information on the specified command. If no command is specified, it
> a list of available commands.
>
> **Usage:** `~help queue`
>
> **Aliases:** h

> ### ~history
>
> Shows the last 10 songs played.

> ### ~lockto
>
> Adds or removes the mentioned channel to/from the allowed list. Users without
> Server won't be able to use the bot outside of these channels. If the allowed
> is empty, the bot can be used anywhere. Without arguments, displays the
> designated channels.
>
> **Usage:** `~lockto #music`

> ### ~move
>
> Moves the target song to the target position in the queue. Specify the target
> and position by index.
>
> **Usage:** `~move 5 2`

> ### ~nowplaying
>
> Shows the currently playing song.
>
> **Aliases:** np

> ### ~pause
>
> Pauses the music player, if it's active.
>
> **Aliases:** stop

> ### ~ping
>
> Shows the bot's latency.

> ### ~play
>
> Starts the queue. If the bot is not in any channels, it will join yours. If you
> a query or URL, it will be queued as well.
>
> **Usage:** `~play muse - hysteria`
>
> **Aliases:** p

> ### ~prefix
>
> Sets the bot's prefix for the server. If no arguments are provided, the current
> is shown instead.
>
> **Usage:** `~prefix p!`

> ### ~queue
>
> Queues a song if a query or URL is given. Otherwise, shows the current queue.
> queue list is paginated. You can specify the page you want by adding it as an
>
> **Usage:** `~queue muse - hysteria`
>
> **Aliases:** q

> ### ~reload
>
> Reloads the specified command. If no command is specified, it reloads all the
> (Bot Owner Only)
>
> **Usage:** `~reload play`

> ### ~repeat
>
> Toggles queue repeating on or off. If on, once a song ends, it's re-added at
> end of the queue.
>
> **Aliases:** loop

> ### ~repeatsong
>
> Toggles song repeating on or off. If on, the current song will repeat forever.
>
> **Aliases:** loopsong

> ### ~removemessages
>
> Removes any messages by Pas within the last 100 messages in the current
>
> **Aliases:** clean

> ### ~resume
>
> Resumes the music player, if it's paused.

> ### ~sysexec
>
> Executes a shell command. Use with extreme caution! (Bot Owner Only)
>
> **Usage:** `~sysexec echo Hello!`
>
> **Aliases:** sh

> ### ~shuffle
>
> Randomly shuffles the queue.

> ### ~skip
>
> Skips the currently playing song.
>
> **Aliases:** next

> ### ~skipto
>
> Skips to and plays the target song. Specify the target song by index. This will
> anything before the target song.
>
> **Usage:** `~skipto 5`

> ### ~restart
>
> Restarts the current song.
>
> **Aliases:** startover

> ### ~topqueue
>
> Queues a song from a query or URL. Songs are placed at the top of the queue
> of the end.
>
> **Usage:** `~topqueue muse - hysteria`
>
> **Aliases:** queuetop

> ### ~unqueue
>
> Un-queues a song. Specify the song by its index in the queue.
>
> **Usage:** `~unqueue 3`
>
> **Aliases:** unq, remove

> ### ~volume
>
> Changes the player's volume. The volume must be a number between and 100 and
> a percentage. If a number is not specified, the current volume will be shown.
>
> **Usage:** `~volume 50`
