import asyncio
import datetime
import os
import re
from asyncio.queues import Queue
from concurrent.futures.thread import ThreadPoolExecutor
from contextlib import suppress

import discord
from youtube_dl import YoutubeDL

try:
    import spotipy
    from spotipy.oauth2 import SpotifyClientCredentials
except ImportError:
    print("warning! 'spotipy' library missing. Spotify support will be disabled.")
    spotipy, SpotifyClientCredentials = None, None

params = {
    'format': 'bestaudio/best',
    'extractaudio': True,
    'audioformat': 'opus',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': True,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'cookiefile': 'static/cookies.txt',
    'source_address': '0.0.0.0'
}


class Stream:
    def __init__(self, cfg):
        self.sp = self.init_spotify(cfg)
        self.params = params
        self.ytdl = YoutubeDL(self.params)
        self.loop = asyncio.get_event_loop()
        self.threads = ThreadPoolExecutor()
        self.queues = {}
        self.players = {}
        self.currents = {}
        self.histories = {}
        self.queue_repeaters = []
        self.song_repeaters = []

    @staticmethod
    def init_spotify(cfg):
        if spotipy is None:
            return

        cid, cs = cfg.sp_client_id, cfg.sp_client_secret
        manager = SpotifyClientCredentials(client_id=cid, client_secret=cs)
        return spotipy.Spotify(auth_manager=manager)

    @staticmethod
    async def queue_to_list(queue):
        queue_list = []
        while not queue.empty():
            item = await queue.get()
            queue_list.append(item)
        for item in queue_list:
            await queue.put(item)
        return queue_list

    def clean_up(self, guild_id):
        if guild_id in self.queues:
            del self.queues[guild_id]
        if guild_id in self.players:
            del self.players[guild_id]
        if guild_id in self.currents:
            del self.currents[guild_id]
        if guild_id in self.queue_repeaters:
            self.queue_repeaters.remove(guild_id)
        if guild_id in self.song_repeaters:
            self.song_repeaters.remove(guild_id)

    def get_queue(self, guild_id):
        queue = self.queues.get(guild_id, Queue())
        self.queues.update({guild_id: queue})
        return queue

    async def queue_song(self, song):
        queue = self.get_queue(song.requester.guild.id)
        await queue.put(song)

    async def top_queue_song(self, song):
        queue = self.get_queue(song.requester.guild.id)
        song_list = [song]
        while not queue.empty():
            item = await queue.get()
            song_list.append(item)
        for item in song_list:
            await queue.put(item)

    async def extract_spotify_info(self, url):
        url_match = re.search(r'https?://open\.spotify\.com/(playlist|album|track)/([^/?&#]+)', url)
        uri_match = re.search('spotify:(playlist|album|track):([^/?&#]+)', url)
        match = url_match or uri_match
        if not match:
            return

        item_type, item_id = match.groups()
        sp_method = getattr(self.sp, item_type)
        try:
            return sp_method(f'spotify:{item_type}:{item_id}')
        except spotipy.SpotifyException:
            return None

    async def extract_info(self, url, yt_playlist=False, process_spotify=True):
        if process_spotify and ('open.spotify.com' in url or 'spotify:' in url):
            return await self.extract_spotify_info(url)

        if yt_playlist:
            self.ytdl.params.update({'noplaylist': False})

        result = await self.loop.run_in_executor(self.threads, self.ytdl.extract_info, url, False)

        if yt_playlist:
            self.ytdl.params.update({'noplaylist': True})

        return result


class Player:
    def __init__(self, cmd, ctx):
        self.cmd = cmd
        self.ctx = ctx
        self.queue = self.cmd.bot.stream.get_queue(self.ctx.msg.guild.id)
        self.cmd.bot.stream.players.update({self.ctx.msg.guild.id: self})

    @classmethod
    def new(cls, bot, ctx):
        return cls(bot, ctx)

    @staticmethod
    def listening(voice_client):
        if not voice_client:
            return False

        listening = 0
        for member in voice_client.channel.members:
            mem = member.voice
            if not any([member.bot, mem.self_deaf, mem.deaf]):
                listening += 1
        return bool(listening)

    def active(self, voice_client):
        if not voice_client:
            return False

        listening = self.listening(voice_client)
        if not listening:
            return False

        playing = voice_client.is_playing()
        paused = voice_client.is_paused()
        if not playing and not paused:
            return False
        return True

    async def start(self):
        curr_song = self.cmd.bot.stream.currents.get(self.ctx.msg.guild.id)

        while not self.queue.empty():
            if not self.ctx.msg.guild.voice_client:
                return

            if curr_song and self.ctx.msg.guild.id in self.cmd.bot.stream.song_repeaters:
                song = curr_song
            else:
                song = await self.queue.get()
                if isinstance(song, SpotifySong):
                    song = await song.convert()
                    if not song:
                        await self.ctx.reply(0xBE1931, '❗ Unable to convert Spotify song. Skipping')
                        continue

            init_msg = await self.ctx.reply(0x3B88C3, f'🔽 Queueing: {song.title}...')
            if not self.ctx.msg.guild.voice_client:
                response = self.ctx.response(0xBE1931, '❗ The voice client seems to have broken')
                await response.send(init_msg)
                return

            connected = False
            connection_attempts = 0
            while not connected and connection_attempts < 3:
                try:
                    await song.play(self.ctx.msg.guild.voice_client)
                    connected = True
                except discord.ClientException:
                    connection_attempts += 1
                    with suppress(Exception):
                        if self.ctx.msg.guild.voice_client:
                            await self.ctx.msg.guild.voice_client.disconnect()
                        await self.cmd.bot.commands.get('connect').execute(self.ctx)

            if not connected:
                self.ctx.reply(0xBE1931, '❗ The voice client was unable to connect')
                return

            volume = self.cmd.bot.db.select(self.ctx.msg.guild.id, 'volume') or 100
            self.ctx.msg.guild.voice_client.source.volume = volume / 100
            self.cmd.bot.stream.currents.update({self.ctx.msg.guild.id: song})
            if not self.cmd.bot.cfg.slim:
                response = self.ctx.response(0x3B88C3)
                response.raw.add_field(name='🎵 Now Playing', value=f'[{song.title}]({song.source_url})')
                response.raw.set_thumbnail(url=song.thumbnail)
                duration = str(datetime.timedelta(seconds=song.duration)).lstrip('0:')
                response.raw.set_footer(text=f'Duration: {duration} | Requester: {song.requester}')
            else:
                response = self.ctx.response(f'✅ Now Playing: {song.title}')
            await response.send(init_msg)

            repeating = False
            while self.active(self.ctx.msg.guild.voice_client):
                if not repeating:
                    if self.ctx.msg.guild.id in self.cmd.bot.stream.queue_repeaters:
                        await self.queue.put(song)
                        repeating = True
                await asyncio.sleep(2)

            history = self.cmd.bot.stream.histories.get(self.ctx.msg.guild.id, [])
            history.insert(0, f'[{self.cmd.bot.utils.shorten(song.title)}]({song.source_url})')
            self.cmd.bot.stream.histories.update({self.ctx.msg.guild.id: history[:10]})

            if os.path.isfile(song.location):
                if self.ctx.msg.guild.voice_client:
                    self.ctx.msg.guild.voice_client.stop()
                    if not self.listening(self.ctx.msg.guild.voice_client):
                        await self.ctx.msg.guild.voice_client.disconnect()
                        self.cmd.bot.stream.clean_up(self.ctx.msg.guild.id)

                await asyncio.sleep(1)
                os.remove(song.location)

        if self.ctx.msg.guild.voice_client:
            await self.ctx.reply(0x3B88C3, '🎵 Queue complete')
            await self.ctx.msg.guild.voice_client.disconnect()
            self.cmd.bot.stream.clean_up(self.ctx.msg.guild.id)


class Song:
    def __init__(self, requester, info):
        self.info = info
        self.requester = requester
        self.url = self.info.get('webpage_url')
        self.source_url = self.url
        self.video_id = self.info.get('id', self.url)
        self.title = self.info.get('title').replace('[', '(').replace(']', ')')
        self.thumbnail = self.info.get('thumbnail', 'https://i.imgur.com/CGPNJDT.png')
        self.duration = int(self.info.get('duration', 0))
        # self.location = None
        self.location = self.info.get('url')
        self.params = params
        self.ytdl = YoutubeDL(self.params)
        self.loop = asyncio.get_event_loop()
        self.threads = ThreadPoolExecutor()
        self.clean_title()

    def clean_title(self): 
        title = self.title
        # if ' - ' in title:
        #     title = title[title.index('-') + 1:]
        if 'official' in title.lower():
            title = title[:title.lower().index('official') - 1]
        self.title = title

    # async def download(self):
    #     if not self.url:
    #         return
    #
    #     out_location = f'cache/{self.video_id}'
    #     if not os.path.exists(out_location):
    #         self.ytdl.params.update({'outtmpl': out_location})
    #         await self.loop.run_in_executor(self.threads, self.ytdl.extract_info, self.url)
    #     self.location = out_location

    async def play(self, voice_client):
        if not voice_client:
            return
        # await self.download()
        # if not self.location:
        #     return

        audio_source = discord.FFmpegPCMAudio(self.location)
        final_source = discord.PCMVolumeTransformer(audio_source)
        if not voice_client.is_playing():
            voice_client.play(final_source)


class SpotifySong:
    def __init__(self, bot, requester, info):
        self.bot = bot
        self.info = info
        self.requester = requester
        self.url = self.info.get('url')
        self.source_url = self.url
        self.title = self.info.get('title')
        self.duration = self.info.get('duration')
        self.thumbnail = self.info.get('thumbnail')

    async def convert(self):
        result = await self.bot.stream.extract_info(self.title.replace(' - ', ' ', 1))
        if result.get('_type') == 'playlist':
            result = result['entries'][0]

        try:
            song = Song(self.requester, result)
            song.source_url = self.url
            return song
        except AttributeError:
            return None


class Query:
    def __init__(self, bot, query, is_multi, is_spotify, is_share, is_url):
        self.bot = bot
        self.query = query
        self.is_multi = is_multi
        self.is_spotify = is_spotify
        self.is_share = is_share
        self.is_url = is_url

    @classmethod
    def parse(cls, bot, query):
        is_multi = False
        is_spotify = False
        is_share = False
        is_url = True
        if 'open.spotify.com' in query:
            is_spotify = True
            query = query.split('&')[0]
            if 'track' not in query:
                is_multi = True
        elif 'spotify:' in query:
            is_spotify = True
            if 'track' not in query:
                is_multi = True
        elif '/watch?' in query:
            query = query.split('&')[0]
        elif '/playlist?' in query:
            is_multi = True
        elif 'youtu.be' in query:
            is_share = True
        elif not query.startswith('http'):
            is_url = False
        if is_url:
            query = query.strip('<>')
        return cls(bot, query, is_multi, is_spotify, is_share, is_url)

    @staticmethod
    def make_track_info(track, images):
        artist = track['artists'][0]['name']
        info = {
            'url': track['external_urls']['spotify'],
            'title': artist + ' - ' + track['name'],
            'duration': int(track['duration_ms'] / 1000),
            'thumbnail': images[-1]['url']
        }
        return info

    async def from_spotify(self, bot, requester, results):
        item_type = results.get('type')
        if item_type == 'playlist':
            to_process = []
            tracks = results['tracks']['items']
            for track in tracks:
                info = self.make_track_info(track['track'], track['track']['album']['images'])
                to_process.append(SpotifySong(bot, requester, info))

        elif item_type == 'album':
            to_process = []
            tracks = results['tracks']['items']
            for track in tracks:
                info = self.make_track_info(track, results['images'])
                to_process.append(SpotifySong(bot, requester, info))

        else:
            info = self.make_track_info(results, results['album']['images'])
            to_process = SpotifySong(bot, requester, info)

        return to_process

    async def try_get_result(self, cmd):
        tries = 0
        results = None
        while not results or tries <= 3:
            _results = await cmd.bot.stream.extract_info(self.query)
            if _results.get('_type') == 'playlist':
                try:
                    results = _results['entries'][0]
                except IndexError:
                    tries += 1
                    await asyncio.sleep(1)

        error = None if results else 'Failed to retrieve song from YouTube'
        return results, error

    async def get_results(self, cmd, ctx):
        error = None

        yt_playlist = not self.is_spotify and self.is_multi
        results = await cmd.bot.stream.extract_info(self.query, yt_playlist=yt_playlist)
        if not results:
            service = 'Spotify' if self.is_spotify else 'YouTube'
            return None, f'Failed to retrieve media from {service}'
        if self.is_spotify:
            return await self.from_spotify(cmd.bot, ctx.msg.author, results), error

        elif results.get('_type') == 'playlist':
            if self.is_share:
                self.is_multi = True
            elif not self.is_multi:
                try:
                    results = results['entries'][0]
                # some kind of rate limit? try the request again.
                except IndexError:
                    results, error = await self.try_get_result(cmd)

        if self.is_multi:
            to_queue = []
            if not self.is_spotify:
                results = results.get('entries')
            songs = list(filter(lambda x: x, results))
            for song in songs:
                to_queue.append(Song(ctx.msg.author, song))

        elif results:
            to_queue = Song(ctx.msg.author, results)

        else:
            to_queue = None

        return to_queue, error
