import errno
import importlib
import json
import os
import shutil
from datetime import datetime

import discord

from core.utils import Utils
from core.command import Command, CommandContext
from core.config import Config
from core.database import Database
from core.logger import create_logger
from core.stream import Stream


class Pas(discord.Client):
    def __init__(self):
        intents = discord.Intents.all()
        super().__init__(intents=intents)
        self.ready = False
        self.utils = Utils()
        self.cfg = Config()
        self.stream = Stream(self.cfg)
        self.commands = {}
        self.alts = {}
        self.log = create_logger('Pas')
        self.log.info('Logger Created')
        self.db = Database(self)
        self.log.info('Connected to Database')
        self.init_cache()
        self.init_commands()
        self.command_md()

    @staticmethod
    def init_cache():
        if not os.path.isfile('static/cookies.txt'):
            open('static/cookies.txt', 'x')
        if os.path.exists('cache'):
            shutil.rmtree('cache')
        os.makedirs('cache')

    def parse_message(self, content, prefix):
        args = list(filter(lambda a: a != '', content))
        if self.cfg.prefix_space:
            args.pop(0)
            cmd = args.pop(0) if args else ""
        else:
            cmd = args.pop(0)[len(prefix):].lower()
        return cmd, args

    def init_commands(self):
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            with open(os.path.join(base_path, 'static/commands.json'), encoding='utf-8') as info_file:
                module_info = json.load(info_file)
                for cmd_info in module_info.get('commands', []):
                    cmd_func = importlib.import_module(f'commands.{cmd_info.get("name")}')
                    importlib.reload(cmd_func)
                    command = Command(self, cmd_func, cmd_info)
                    self.commands.update({command.name: command})
                    for alt in command.alts:
                        self.alts.update({alt: command.name})
        except FileNotFoundError:
            print('file \'commands.json\' not found!')
            exit(errno.ENOENT)

    def command_md(self):
        pfx = self.cfg.prefix
        cmd_blocks = []
        for cmd in self.commands.values():
            cmd_block = f'> ### {pfx}{cmd.name}\n>\n'
            self.utils.make_sentences(cmd.desc.split())
            cmd_block += self.utils.make_sentences(cmd.desc.split())
            if cmd.usage:
                cmd_block += f'>\n> **Usage:** `{pfx}{cmd.name} {cmd.usage}`\n'
            if cmd.alts:
                cmd_block += f'>\n> **Aliases:** {", ".join(cmd.alts)}\n'
            cmd_blocks.append(cmd_block)
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        with open(os.path.join(base_path, 'static/commands.md'), 'w+', encoding='utf-8') as md_file:
            md_text = '## Commands\n\n' + '\n'.join(cmd_blocks)
            md_file.write(md_text)

    async def on_ready(self):
        self.ready = True
        self.log.info(f'Logged in as {self.user.name}')
        self.log.info(f'Date: {datetime.utcnow().strftime("%d. %B %Y %H:%M:%S")}')
        self.log.info('---------------------------------')

    async def on_message(self, message):
        if all([self.ready, message.guild, not message.author.bot]):
            channels = self.db.select(message.guild.id, 'channels')
            if channels and str(message.channel.id) not in channels.split(','):
                if not message.author.guild_permissions.manage_guild:
                    await message.add_reaction('🔒')
                    return
            prefix = self.db.prefix(message.guild.id)
            if not message.content.startswith(prefix):
                return
            cmd, args = self.parse_message(message.content.split(), prefix)
            if cmd in self.alts:
                cmd = self.alts[cmd]
            if cmd not in self.commands:
                return

            command = self.commands.get(cmd)
            ctx = CommandContext(message, args, command.bot.cfg)
            await command.execute(ctx)

    def run(self):
        try:
            self.log.info('Connecting...')
            super().run(self.cfg.token, bot=True)
        except discord.LoginFailure:
            self.log.error('Invalid Token!')
            exit(errno.EPERM)
