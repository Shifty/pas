import json
import os
import errno


class Version:
    def __init__(self):
        self.raw = self._load_file()
        self.timestamp = self.raw.get('build_date', 0)
        self.version = self.raw.get('version', {})

    @staticmethod
    def _load_file():
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        file_path = 'static/version.json'
        try:
            with open(os.path.join(base_path, file_path), encoding='utf-8') as config:
                return json.load(config)
        except FileNotFoundError:
            print('file \'version.json\' not found!')
            exit(errno.ENOENT)


class Config:
    def __init__(self):
        self.raw = self._load_file()
        self.debug = self.raw.get('debug')
        self.slim = self.raw.get('slim')
        self.prefix_space = self.raw.get('prefix_space')
        self.token = self.raw.get('token')
        self.prefix = self.raw.get('prefix')
        self.owners = self.raw.get('owners')
        self.error_channel = self.raw.get('error_channel')
        self.sp_client_id = self.raw.get('sp_client_id')
        self.sp_client_secret = self.raw.get('sp_client_secret')
        self.version = Version()

    @staticmethod
    def _load_file():
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        file_path = 'static/config.json'
        try:
            with open(os.path.join(base_path, file_path), encoding='utf-8') as config:
                return json.load(config)
        except FileNotFoundError:
            print('file \'config.json\' not found!')
            exit(errno.ENOENT)
