import logging

from datetime import datetime

fmt = '[ {levelname:^8s} | {asctime:s} | {name:<25.25s} ] {message:s}'
date_fmt = '%Y.%m.%d %H:%M:%S'
loggers = {}


def create_logger(name):
    if name not in loggers.keys():
        file = f'logs/pas.{datetime.utcnow().strftime("%Y-%m-%d")}.log'
        logger = logging.getLogger(name)
        logger.setLevel(logging.INFO)
        # Add File Handler
        handler = logging.FileHandler(file, encoding='utf-8')
        handler.setLevel(logging.INFO)
        handler.setFormatter(logging.Formatter(fmt=fmt, datefmt=date_fmt, style='{'))
        logger.addHandler(handler)
        # Add Stream Handler
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(fmt=fmt, datefmt=date_fmt, style='{'))
        logger.addHandler(handler)
        # Cache Logger
        loggers.update({name: logger})
    logger = loggers.get(name)
    return logger
