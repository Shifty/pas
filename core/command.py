import traceback
from contextlib import suppress

import discord

from core.logger import create_logger


class DummyException(Exception):
    def __init__(self):
        self.message = 'This is a dummy exception, it should never be raised.'


class CommandRequirements:
    def __init__(self, cmd, message):
        self.msg = message
        self.chn = self.msg.channel
        self.req = cmd.requirements
        self.req_met = True
        self.missing = []
        self.check_requirements()

    def check_requirements(self):
        if not self.msg.guild:
            return

        pd = dict(self.msg.guild.me.permissions_in(self.chn))
        self.missing = [r for r in self.req if not pd.get(r)]
        self.req_met = not bool(self.missing)


class Response:
    def __init__(self, ctx, color, title, force_embed=False):
        self.ctx = ctx
        self.raw = None
        self.kwarg = None
        self.color = color
        self.title = title
        self.force_embed = force_embed
        self.set_raw()

    def set_raw(self):
        # noinspection PyProtectedMember
        if not self.ctx._cfg.slim or self.force_embed:
            embed = discord.Embed(color=self.color)
            if self.title:
                embed.title = self.title
            self.raw = embed
            self.kwarg = 'embed'
        else:
            self.raw = self.title
            self.kwarg = 'content'

    async def send(self, init_msg=None):
        try:
            msg = await init_msg.edit(**{self.kwarg: self.raw})
        except (discord.NotFound, AttributeError):
            msg = await self.ctx.msg.channel.send(**{self.kwarg: self.raw})
        return msg


class CommandContext:
    def __init__(self, msg, args, cfg):
        self.msg = msg
        self.args = args
        self._cfg = cfg

    def response(self, color=None, title=None, force_embed=False):
        return Response(self, color, title, force_embed)

    @staticmethod
    def get_avatar(item):
        if not item.avatar:
            return item.default_avatar_url
        av = item.avatar_url
        if item.avatar.startswith('a_'):
            av = '.'.join(av.split('.')[:-1]) + '.gif?size=1024'
        return av

    async def reply(self, color, title):
        if not self._cfg.slim:
            embed = discord.Embed(color=color, title=title)
            msg = await self.msg.channel.send(embed=embed)
        else:
            msg = await self.msg.channel.send(title)
        return msg


class Command:
    def __init__(self, bot, command, info):
        self.bot = bot
        self.info = info
        self.command = command
        self.alts = info.get('alts', [])
        self.name = info.get('name')
        self.usage = self.info.get('usage', '')
        self.desc = self.info.get('description')
        self.owner = bool(self.info.get('owner'))
        if self.owner:
            self.desc += '\n(Bot Owner Only)'
        self.req = self.info.get('requirements', [])
        self.requirements = ['send_messages', 'embed_links', *self.req]
        self.log = create_logger(self.name.upper())

    def get_exception(self):
        return DummyException if self.bot.cfg.debug else Exception

    def log_usage(self, message, args):
        server = f'SRV: {message.guild.name} [{message.guild.id}]'
        channel = f'CHN: #{message.channel.name} [{message.channel.id}]'
        author = f'{message.author.name}#{message.author.discriminator} [{message.author.id}]'
        entry = f'USR: {author} | {server}| {channel}'
        if args:
            entry += f' | ARGS: {" ".join(args)}'
        self.log.info(entry)

    async def send_error_message(self, err):
        error_channel = self.bot.get_channel(self.bot.cfg.error_channel)
        if not error_channel:
            return

        error_text = f'Command **{self.name}** raised an exception: `{err.__class__.__name__}`\n'
        error_text += f'```py\n{traceback.format_exc()[:1800]}\n```'
        try:
            await error_channel.send(error_text)
        except discord.Forbidden:
            pass

    async def send_req_message(self, requirements, message):
        req_embed = discord.Embed(color=0xBE1931)
        title = '❗ Missing permissions'
        unmet_list = ''
        for req in requirements.missing:
            req = req.replace('_', ' ').title()
            unmet_list += f'\n- {req}'
        req_embed.add_field(name=title, value=f'```\n{unmet_list}\n```')
        prefix = self.bot.db.prefix(message.guild.id)
        req_embed.set_footer(text=f'{prefix}{self.name} could not execute')
        with suppress(discord.NotFound, discord.Forbidden):
            await message.channel.send(embed=req_embed)

    async def execute(self, cfg):
        self.log_usage(cfg.msg, cfg.args)
        requirements = CommandRequirements(self, cfg.msg)
        if self.owner:
            if cfg.msg.author.id not in self.bot.cfg.owners:
                await cfg.msg.add_reaction('⛔')
                return
        if not requirements.req_met:
            await self.send_req_message(requirements, cfg.msg)
            return
        try:
            await getattr(self.command, self.name)(self, cfg)
        except self.get_exception() as e:
            self.bot.log.error(f'ERROR: {repr(e)} ')
            await self.send_error_message(e)
            with suppress(discord.Forbidden, discord.NotFound):
                await cfg.msg.add_reaction('❗')
