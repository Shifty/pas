import errno
import json
import os
import sqlite3


class Table:
    def __init__(self, raw):
        self.name = raw.get('name')
        self.primary = raw.get('primary')
        self.columns = raw.get('columns')

    @property
    def col_text(self):
        col_lines = []
        for name, (types, options) in self.columns.items():
            col_lines.append(f'{name} {types} {options}')
        return ', \n'.join(col_lines)


class Database:
    def __init__(self, bot):
        self.raw = self._load_file()
        self.bot = bot
        self.conn = sqlite3.connect('static/pas.db')
        self.curr = self.conn.cursor()
        self.tables = {n: Table(t) for n, t in self.raw.get('tables').items()}
        self.settings = self.tables.get('settings')
        self.queues = self.tables.get('queues')
        self.init_table()

    @staticmethod
    def _load_file():
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        file_path = 'static/database.json'
        try:
            with open(os.path.join(base_path, file_path), encoding='utf-8') as config:
                return json.load(config)
        except FileNotFoundError:
            print('file \'database.json\' not found!')
            exit(errno.ENOENT)

    def init_table(self):
        statement = (
            f'CREATE TABLE IF NOT EXISTS {self.settings.name} ( '
            f'{self.settings.col_text}'
            ');'
        )
        self.curr.execute(statement)
        self.conn.commit()

    def prefix(self, guild_id):
        default = self.bot.cfg.prefix
        current = self.select(guild_id, 'prefix')
        return current or default

    def exists(self, identifier):
        statement = (
            f'SELECT EXISTS (SELECT 1 FROM {self.settings.name} '
            f'WHERE {self.settings.primary} = ?)'
        )
        self.curr.execute(statement, (identifier,))
        return bool(self.curr.fetchone()[0])

    def select(self, identifier, column='*'):
        statement = (
            f'SELECT {column} FROM {self.settings.name} '
            f'WHERE {self.settings.primary} = ? '
            'LIMIT 1'
        )
        self.curr.execute(statement, (str(identifier),))
        result = self.curr.fetchone()
        if result and len(result) == 1 and column:
            result = result[0]
        return result

    def _insert(self, identifier, **kwargs):

        non_primaries = list(self.settings.columns.keys())
        non_primaries.remove(self.settings.primary)

        params = (str(identifier),)
        for param in non_primaries:
            value = kwargs.get(param)
            params += (value,)

        statement = (
            f'INSERT INTO {self.settings.name} ({", ".join(self.settings.columns)}) '
            f'VALUES ({", ".join(["?"] * len(self.settings.columns))})'
        )
        self.curr.execute(statement, params)
        self.conn.commit()

    def update(self, identifier, **kwargs):
        if not self.exists(str(identifier)):
            self._insert(str(identifier), **kwargs)
            return

        statement = f'UPDATE {self.settings.name} '

        non_primaries = list(self.settings.columns.keys())
        non_primaries.remove(self.settings.primary)

        params = ()
        for param in non_primaries:
            value = kwargs.get(param)
            if value:
                statement += f'SET {param} = ? '
                params += (value,)

        params += (str(identifier),)
        statement += f'WHERE {self.settings.primary} = ?'

        self.curr.execute(statement, params)
        self.conn.commit()

    def delete(self, identifier):
        statement = (
            f'DELETE FROM {self.settings.name} '
            f'WHERE {self.settings.primary} = ?'
        )
        self.curr.execute(statement, (str(identifier),))
        self.conn.commit()
