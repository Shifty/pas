class Utils:
    @staticmethod
    def shorten(text, max_len=45, appendage='...'):
        if len(text) > max_len:
            text = text[:max_len] + appendage
        return text

    @staticmethod
    def make_sentences(desc):
        lines = []
        line = []
        while desc:
            word = desc.pop(0)
            if len(' '.join(line)) + len(word) <= 78:
                line.append(word)
                if not desc:
                    lines.append(' '.join(line))
            else:
                lines.append(' '.join(line))
                line.clear()
        return '> ' + '\n> '.join(lines) + '\n'

    @staticmethod
    def paginate(items, pg_num, span=10):
        try:
            page = abs(int(pg_num))
        except (ValueError, TypeError):
            page = 1
        pages, length = len(items) // span, len(items)
        max_page = pages if length % span == 0 and length != 0 else pages + 1
        page = max_page if page > max_page != 0 else page if page else 1
        start_range, end_range = (page - 1) * span, page * span
        return items[start_range:end_range], page
